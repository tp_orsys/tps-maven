# TPs Maven



## Structure

Ce dossier est un projet Git. Il contient les questions et les solutions des TPs.

Chaque exercice, chaque solution fait l'objet d'une branche Git.

Chaque exercice contient son propre fichier README.md. Quand on change de branche, on obtient l'énoncé correspondant et on a la configuration initiale pour l'exercice.

En particulier, les fichiers qu'il faut utiliser pour l'exercice sont dans le dossier `assets`.

Nous construirons plusieurs projets Maven dans ce dossier.

## Mode d'emploi

Nous utiliserons la ligne de commande pour simplifier les problèmes liés à des IDE différents. Toutes les instructions de création ou de copie de dossier ou de fichiers sont données avec une syntaxe Unix. Adapter pour Windows.

Il peut être plus pratique de lire les énoncés sur GitLab, sauf si on dispose d'un lecteur de documents *mark down* sur sa machine.

Cependant, on effectue les exercices sur sa propre machine. Pour cela, il convient de *cloner* le projet par la commande (Unix comme Windows)

```
git clone https://gitlab.com/tp_orsys/tps-maven.git
```

Cette commande crée un dossier nommé `tps-maven`. Il convient de placer la ligne de commande dans ce dossier ou un sous-dossier pour effectuer toutes les commandes Git dans la suite des TPs.

Ensuite, on change d'exercice sur GitLab en cliquant le lien correspondant dans la table des matières ci-dessous pour obtenir l'énoncé et on change de branche sur sa machine pour obtenir la configuration initiale de l'exercice par la commande git :

```
git checkout exo_xx
```

où xx est le numéro de la question sur deux chiffres, par exemple `git checkout exo_03`

Il est préférable de créer une branche spécifique pour sa propre solution.

On peut repartir à chaque fois de la branche fournie ou continuer sa propre solution.

De plus, des exercices supplémentaires non corrigés sont fournis.

## Rappels concernant Git

### Présentation générale

Git est un *gestionnaire de sources*. On peut le voir comme un *annuler/refaire* sur des ensembles de fichiers. Git permet de gérer des *branches* de développement indépendamment l'une de l'autre.

Ainsi typiquement on aura une branche `main` qui contiendra uniquement les livrables, une branche `develop` qui contient le travail en cours validé et une branche par fonctionnalité ou correctif de bug. La branche `develop` sera fusionnée sur la branche `main` à chaque fois qu'on aura atteint une nouvelle version qu'on souhaite livrer, les branches de fonctionnalités ou de correctifs seront fusionnées sur la branche `develop` quand la fonctionnalité ou le correctif est validé.

Git est très utile pour le travail sur un poste individuel et extrêmement pratique pour le travail collaboratif. Ici nous n'utiliserons cet outil que pour du travail sur le poste individuel.

### Aperçu très superficiel du fonctionnement

Git gère des *commits*. Un *commit* est comme une photo de l'ensemble des fichiers gérés.

Un *commit connaît* son ou ses parents immédiats.

Un *commit* est *immutable* : on ne peut pas le modifier.

Une branche est vue comme une succession de *commits* mais en fait ce n'est qu'un *pointeur* sur un *commit* qui est l'extrêmité de la branche.

L'identificateur `HEAD` pointe sur la branche courante.

### Principales commandes de Git

La plupart de ces commandes ont des options qui permettent de particulariser leur comportement.

- `git log` : affiche la suite des *commits* de la branche courante
- `git clone <url d'un projet>` : *clone* un projet ; toutes les branches d'un projet sont téléchargées localement dans un nouveau dossier
- `git checkout <nom de branche>` : sélection d'une branche déjà existante
- `git branch` : liste les branches. L'option -a montre aussi les branches distantes dans le cas d'un projet synchronisé avec un projet distant.
- `git branch <nom de branche>` : création d'une branche mais sans changement de branche courante
- `git checkout -b <nom de branche>` : création d'une branche et sélection de celle-ci
- `git add <nom de fichier ou de dossier>` : ajout des fichiers et dossiers dans *l'index*
- `git commit` : enregistrement de l'index dans un nouveau *commit*
- `git commit -m "<message>"` : enregistrement de l'index dans un nouveau *commit*
- `git merge <nom de la branche>` : fusion des modifications de la branche nommée dans la branche courante
- `git pull` : met à jour la branche locale en fusionnant la branche correspondante du projet distant.

### Raccourcis

On peut ajouter dquelques raccourcis usuels à Git en exécutant les commandes :

```
git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.ci commit
git config --global alias.st status
git config --global alias.ln "log --no-decorate --pretty=oneline"
```

Avec ces raccourcis, on écrit `git br <nom-de-branche>` au lieu de `git branch <nom-de-branche>`

### Bonnes pratiques 

- toujours effectuer une modification sur une branche dédiée

- ne fusionner qu'une fois la branche validée (`merge` ou `merge request`, `pull request` selon le cas)

## Table des matières

- [Accueil](https://gitlab.com/tp_orsys/tps-maven)
- [1- Premier projet Maven](https://gitlab.com/tp_orsys/tps-maven/-/tree/exo_01)
- [2- Création d'une bibliothèque et tests unitaires](https://gitlab.com/tp_orsys/tps-maven/-/tree/exo_02)
- [3- Création d'une application web, paramètres de configuration, utilisation directe d'un plugin](https://gitlab.com/tp_orsys/tps-maven/-/tree/exo_03)
- [4- Création d'un projet *Reactor*](https://gitlab.com/tp_orsys/tps-maven/-/tree/exo_04)
- [5- Tests d'intégration](https://gitlab.com/tp_orsys/tps-maven/-/tree/exo_05)
- [6- Archétype](https://gitlab.com/tp_orsys/tps-maven/-/tree/exo_06)

Exercices non corrigés :

- [11- Assembly](https://gitlab.com/tp_orsys/tps-maven/-/tree/exo_11)
- [12- Site](https://gitlab.com/tp_orsys/tps-maven/-/tree/exo_12)
- [13- Jenkins](https://gitlab.com/tp_orsys/tps-maven/-/tree/exo_13)
- [21- Création d'un *plugin*](https://gitlab.com/tp_orsys/tps-maven/-/tree/exo_21)
- [22- Sonar](https://gitlab.com/tp_orsys/tps-maven/-/tree/exo_22)

[Références](https://gitlab.com/tp_orsys/tps-maven/-/tree/references)



© JL Déléage 2022

formation@leuville.com
